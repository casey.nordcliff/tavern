# Welcome to Tavern!

## Intro
Tavern is a multi-platform dektop app that makes it fun and easy to keep track of
your favorite team-based dice rolling tabletop game - Dungeons and Dragons!

Features are going to be coming as they will, which is basically when I have time,
but possible ideas include:

1) Dice rolling
2) Save / load character sheets
3) Random character generator
4) Map viewer
5) Random map generator

## Architecture
Long story short, all the cool stuff is in the `ui/` folder. It contains a lot of React
components. The `package.json` file in the root of the project has a script called
`ui:compile` that you can call to build it. The result is `ui/build/` which has all of
the static content in it. Once thats done, we start up electron, and we point it to
`ui/build/index.html`, and that loads the UI. From there, you use the app, simple.

## Design Pattern / Philosophy
Tavern will always be 100% free-as-in-money, free-as-in-liberty, open source, friendly,
ad-free, not too bloated (I know it's electron - cut me some slack) and helpful. It will
be forward and obvious with its intentions, and not too quick to throw in the kitchen
sink with what it includes feature-wise.

Stack wise, I'd like to stick to TypeScript and React as much as possible, and we can add
things as we see necessary.

## Contributing
 If you have an idea or experience to offer, great! The more the merrier!
 * If you have an issue, make a new issue on GitLab with a detailed description of what the issue is
 * If you have a feature request, do the above, and put `FEATURE-REQUEST` in the title somewhere.
 * If you have new code, make a PR, I'll see it

 Maybe I'll get around to making a discord, maybe not.