const { app, BrowserWindow } = require('electron')
const path = require('path')

function createWindow() {
    const mainWindow = new BrowserWindow({
        height: 600,
        width: 800,
        minHeight: 600,
        minWidth: 800,
        webPreferences: {
            nodeIntegration: true
        },

        // Credit to Nikita Golubev via https://www.flaticon.com/free-icon/beer_1428293
        icon: 'assets/icon.png'
    })

    // Load the app content
    process.env.DEV_MODE === true ?
        mainWindow.loadURL('http://localhost:3000') :
        mainWindow.loadFile(path.join(__dirname, "ui/public/index.html"))
}

app.on("ready", () => {
    createWindow()
    app.on("activate", () => {
        if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })
})

// Closing the app via red x button doesnt quit the process on MacOS
app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit()
    }
})